#!/usr/bin/env python
import rospy
import tf
import numpy as np
from sensor_msgs.msg import LaserScan
from ar_track_alvar_msgs.msg import AlvarMarkers
from tagtracker import Tagtracker
from go_to import *
from explore import *

MAX_DETECTION = 2

class Main:
    def __init__(self):
        self.explorer = Explore(self)
        self.tracker = Tagtracker(self)
        self.discovered_tags = []
        self.is_tag_detected = False
        self.should_get_out = False

        rospy.Subscriber('ar_pose_marker', AlvarMarkers, self.set_ar_data)
        rospy.wait_for_message('ar_pose_marker', AlvarMarkers)
        print "Main: Initialised class"


    def set_ar_data(self, data):
        if not data:
            raise Exception('Main: No data from ar pose marker')
        self.is_tag_detected = bool(data.markers)


    def go_to_center(self, args):
        if not args:
            print "Main: ERROR - USAGE: main.py 'x' 'y' 'theta'"
            return False

        x = float(args[0])
        y = float(args[1])
        theta = float(args[2])

        position = {'x': x, 'y' : y}
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : np.sin(theta/2.0), 'r4' : np.cos(theta/2.0)}
        rospy.loginfo("Go to center: (%s, %s) pose", position['x'], position['y'])

        navigator = GoToPose()
        success = navigator.goto(position, quaternion)
        if success:
            rospy.loginfo("Reached coordinates.")
        else:
            rospy.loginfo("Couldn't reach.")
        return success


    def start(self):
        print "MAIN: START searching"
        # keep exploring the map unless both tags are detected
        while len(self.discovered_tags) < MAX_DETECTION:
            if self.is_tag_detected and not self.should_get_out:
                self.tracker.go_to_tag()
            else:
                self.explorer.explore()

        print "MAIN: Successfully discovered %d tags" % MAX_DETECTION


if __name__ == '__main__':
    try:
        rospy.init_node('main_node', anonymous=True)
        rate = rospy.Rate(10) #10hz
        robot = Main()

        args = rospy.myargv()[1:]
        robot.go_to_center(args)
        robot.start()

        #Sleep to give the last log messages time to be sent
        rospy.sleep(1)


    except rospy.ROSInterruptException:
        rospy.loginfo("Ctrl-C caught. Quitting")
