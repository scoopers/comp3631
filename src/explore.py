#!/usr/bin/env python
import rospy
import random
import time
from math import radians
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan


class Explore:
    def __init__(self, robot):
        self.robot = robot
        self.laser_data = None
        self.rate = rospy.Rate(10)

        self.pub = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size=10)
        self.desired_velocity = Twist()
        self.desired_velocity.linear.x = 0.2 # Forward with 0.2 m/sec.

        self.desired_radians = Twist()
        self.desired_radians.angular.z = 0.4

        rospy.Subscriber('scan', LaserScan, self.set_laser_data)
        rospy.wait_for_message('scan', LaserScan)
        print "EXPLORE: Initialised class"


    def set_laser_data(self, data):
        self.laser_data = data;


    def is_too_close(self):
        if not self.laser_data:
            raise Exception("EXPLORE: Received no data from LaserScan")

        # length of msg spans over the cone of vision of camera
        ranges = self.laser_data.ranges
        if not len(ranges):
            print "EXPLORE: No ranges defined"
            return False

        # middle is forward
        forward = len(ranges) / 2
        distances = [ranges[639], ranges[forward], ranges[0]]
        left = distances[0] < 0.8
        middle = distances[1] < 0.5
        right = distances[2] < 0.8

        if left or middle or right:
            print "EXPLORE: Robot is too close to an object."

        return bool(left or middle or right)


    def spin(self):
        # random.seed(time.time())
        # numbers = range(80, 120) + range(210, 270)
        # amount = int(round(radians(random.choice(numbers)) / abs(self.desired_radians.angular.z)) * 10)
        amount = 45
        print "EXPLORE: Turning %d and going forward" % amount
        for i in range(amount):
            self.pub.publish(self.desired_radians)
            self.rate.sleep()
        self.robot.should_get_out = False


    def explore(self):
        if self.is_too_close():
            self.spin()
        else:
            self.pub.publish(self.desired_velocity)
            self.rate.sleep()


    def stop(self):
        print "EXPLORE: STOP searching"
