#!/usr/bin/env python
import rospy
import numpy as np
import rospkg
import cv2
import glob
import time
import re
import os
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image

class ImageMatcher:
    def __init__(self, tracker):
        self.tracker = tracker
        self.detected = False
        self.bridge = CvBridge()
        r = rospkg.RosPack()
        self.path = r.get_path('real_project')
        self.files = glob.glob(self.path+"/images/*")
        self.template = self.path + "/images/revolver.png"
        self.rate = rospy.Rate(10)
        rospy.Subscriber('/camera/rgb/image_raw', Image, self.callback, queue_size=1, buff_size=2**24)
        print "DETECTOR: Initialised class"

    def callback(self, data):
        self.feed = data

    def detect(self):
        data = self.feed
        start = time.time()
        for f in self.files:
            template = cv2.imread(f)
            # extract object name from file name
            name = re.search('images\/(.+?)\.', f).group(1)
            for i in range(10):
                try:
                    vid_src = self.bridge.imgmsg_to_cv2(data,"bgr8")
                except CvBridgeError as e:
                    print(e)
                cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
                cv2.cvtColor(vid_src, cv2.COLOR_BGR2GRAY)
                MIN_MATCH_COUNT = 176

                # Initiate ORB detector
                orb = cv2.ORB(nfeatures=1000)

                # find the keypoints and descriptors with ORB
                kp1, des1 = orb.detectAndCompute(template,None)
                kp2, des2 = orb.detectAndCompute(vid_src,None)

                # find matches
                search_params = dict(checks = 100)
                FLANN_INDEX_LSH = 6
                index_params= dict(algorithm = FLANN_INDEX_LSH,
                           table_number = 6, # 12
                           key_size = 12,     # 20
                           multi_probe_level = 1) #2
                flann = cv2.FlannBasedMatcher(index_params, search_params)
                matches = flann.knnMatch(des1,des2,k=2)

                # store all the good matches
                good = []
                for v in matches:
                # check if two neighbours even exist
                  if len(v) != 2:
                    continue
                  (m, n) = v
                  # ratio test
                  if m.distance < (0.82 * n.distance):
                      good.append(m)

                if len(good) > MIN_MATCH_COUNT:
                    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
                    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)

                    # find homography using ransac
                    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
                    # get width and height from template image
                    h,w = template.shape[:2]
                    # use to construct points for bounding box
                    pts = np.float32([[0,0], [0,h-1], [w-1,h-1], [w-1,0]]).reshape(-1,1,2)
                    # transform box to fit perspective of source image (needs at least 4 matches)
                    dst = cv2.perspectiveTransform(pts,M)

                    # draw bounding box onto image (more like a bounding polygon)
                    cv2.polylines(vid_src, [np.int32(dst)], 1, 255, 4)

                    # write text on image
                    cv2.putText(
                        vid_src,                    # src image,
                        name,                       # text
                        (30,30),                    # bottom left conrner of text
                        cv2.FONT_HERSHEY_SIMPLEX,   # font
                        1,                          # font scale
                        (0,255,0),                  # font colour
                        2                           # line type
                    )

                    # write screenshot of detected image to file
                    cv2.imwrite(self.path + "/detected/" + name + "-detected.jpg", vid_src)
                    print "____DETECTOR: Match on {}- {}/{}____".format(name, len(good), MIN_MATCH_COUNT)
                    end = time.time()
                    print end - start
                    return True

                else:
                    rospy.loginfo("No match on {}- {}/{}".format(name,len(good),MIN_MATCH_COUNT))
                    matchesMask = None

                cv2.imshow('Matching', vid_src)
                cv2.waitKey(1)
                self.rate.sleep()

        end = time.time()
        print end - start
        print "DETECTOR: failed attempt to detect this tag %d/%d" % (self.tracker.failed_count, self.tracker.MAX_FAIL_ALLOWED)
        return False
