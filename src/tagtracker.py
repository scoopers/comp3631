#!/usr/bin/env python
from __future__ import division
import numpy as np
import math
import tf
import time
import rospy
import sys

from geometry_msgs.msg import Twist, Vector3, PointStamped
from sensor_msgs.msg import Image
from ar_track_alvar_msgs.msg import AlvarMarkers
from multiple_feature_homography import ImageMatcher
from go_to import GoToPose


class Tagtracker:
	def __init__(self, robot):
		self.rate = rospy.Rate(10)
		self.robot = robot
		self.navigator = GoToPose()
		self.detector = ImageMatcher(self)
		self.tf_listener = tf.TransformListener()
		self.failed_count = 0
		self.MAX_FAIL_ALLOWED = 2
        print "TRACKER: Initialised class"


	def go_to_tag(self):
		print "TRACKER: Detected an AR tag. Going towards it"
		try:
			# get quaternion of map in respect to ar marker
			(trans, rot) = self.tf_listener.lookupTransform('map', '/ar_marker_0', rospy.Time(0))

			# check if we've been here before
			if self.already_discovered(trans):
				print "TRACKER: Oops, we've already discovered this tag. Ignoring it..."
				self.robot.should_get_out = True
				return

			self.robot.should_get_out = False

			# unpack into xyzw
			r1 , r2, r3, r4 = rot

			p = PointStamped()
			new_p = PointStamped()
			p.header.frame_id = 'ar_marker_0'
			p.header.stamp = rospy.Time(0)
			p.point.x = 0.0
			p.point.y = 0.0
			p.point.z = 0.4

			new_p = self.tf_listener.transformPoint('map', p)

			theta = 0
			position = {'x': new_p.point.x, 'y' : new_p.point.y}
			quaternion = {'r1' : 0.000, 'r2' : 0.00, 'r3' : r3, 'r4' : r4}

			success = self.navigator.goto(position, quaternion)
			if success:
				print "TRACKER: Reached the AR tag"
				if self.failed_count > self.MAX_FAIL_ALLOWED:
					print "DETECTOR: FAILED %d times. we'll ignore this tag from now on." % self.failed_count
				# do the image recognitions and if successfull store trans as the position of a detected ar tag
				if self.detector.detect() or self.failed_count > self.MAX_FAIL_ALLOWED:
					self.failed_count = 0
					self.robot.discovered_tags.append(trans)
				else:
					self.failed_count += 1

			else:
				print "TRACKER: Failed going towards AR tag. Needs to get out of here..."

		except(tf.LookupException, tf.ExtrapolationException, tf.ConnectivityException):
			rospy.loginfo("Tag not found")
			self.robot.should_get_out = True
			pass

		self.rate.sleep()


	def already_discovered(self, transform):
		for tag in self.robot.discovered_tags:
			# if it's within a certain distance of previously discovered artag, we assume it's been visited
			d = math.sqrt((tag[0] - transform[0]) ** 2 + (tag[1] - transform[1]) ** 2 + (tag[2] - transform[2]) ** 2)
			if d < 1:
				return True
		return False
