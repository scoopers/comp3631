# Note
This project has been reuploaded to GitHub from a private GitLab repository belonging to my University. I do not control it so cannot make it public.
The project was created by a team, so not all contributions are mine.

## Group 14 Robotics Project  

[Watch the video](https://gitlab.com/scoopers/comp3631/IMG_4823.MP4)

## Installation instructions
### Running the project in simulation  
1. Let gazebo know to use sim.world `export TURTLEBOT_GAZEBO_WORLD_FILE=<full-path-to-sim-world>`  
2. Run turtlebot gazebo `roslaunch turtlebot_gazebo turtlebot_world.launch`
3. Run simulated localisation `roslaunch lab4 simulated_localisation.launch map_file:=<path-to-yaml-file>` or `roslaunch turtlebot_gazebo amcl_demo.launch map_file:=<path-to-yaml-file>`
4. Run rviz and estimate starting pose `roslaunch turtlebot_rviz_launchers view_navigation.launch`  
5. Start project launch file `roslaunch real_project cluedo.launch x:=<x-coordinate> y:=<y-coordinate> theta:=<theta-position>`  
## Running the project on a real turtlebot  
1. Run minimal launch file `roslaunch turtlebot_bringup minimal.launch`  
2. Run 3D sensor launch file `roslaunch astra_launch astra.launch`  
3. Run localisation `roslaunch turtlebot_navigation amcl_demo.launch map_file:=<path-to-yaml-file>`  
4. Run rviz and estimate starting pose `roslaunch turtlebot_rviz_launchers view_navigation.launch`  
5. Start project launch file `roslaunch real_project cluedo.launch x:=<x-coordinate> y:=<y-coordinate> theta:=<theta-position>`  
